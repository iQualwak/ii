### Project idea
Our idea will be based on the following open api: https://developer.marvel.com/.
It supposed to be Marvel library.


### How to run
1. Clone/Download from repository.
2. Run `npm start` from the root directory of the project.


### Code style
_in progress (but will be in different .md file_

### How to work with tickets/repository
There are several rules:

1. Do not push directly to master.
2. Do not push directly to dev.
3. All changes should go through dev to master.
4. If you want your changes to be appeared in dev you **should** create a pull-request.
5. Name of pull-request is an ID of ticket you have done.
6. _in progress_
7. _in progress_



### Team:
1. Ilia Prokopev
2. Artem Kozlov
3. Igor Vakhula