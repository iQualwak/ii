const pkg = require('./package.json')

module.exports = {
    "apiPath": "stubs",
    "webpackConfig": {
        "output": {
            "publicPath": `/static/ii/${pkg.version}/`
        }
    },
    "navigations": {
        "repos": "/ii"
    },
    "config": {
        "ii.api.base": "/api"
    }
}