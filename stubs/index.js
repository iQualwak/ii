const router = require('express').Router();

router.get('/characters', (req, res) => {
    // const answer = {
    //     message: 'hello world (from ii team)'
    // }
    
    res.send(require("./characters.json"));
})

router.get('/characters/*', (req, res) => {
    res.send(require("./characters.json"))
});

module.exports = router;