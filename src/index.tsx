import React from 'react';
import ReactDOM from 'react-dom';
import App from "@main/App";

export default () => <App/>

export const mount = (Component) => {

    ReactDOM.render(
        <Component/>,
        document.getElementById('app')
    )
}

export const unmount = () => {
    ReactDOM.unmountComponentAtNode(document.getElementById('app'))
}