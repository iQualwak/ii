import React from 'react';
import Header from './components/main/Header';
import Body from './components/main/Body';
import {Provider} from 'react-redux';
import applicationStore from '@main/store/store';
import Footer from "@main/components/main/Footer";

const App: React.FC = () => (
    <Provider store={applicationStore}>
        <Header />
        <Body />
        <Footer />
    </Provider>
);

export default App;