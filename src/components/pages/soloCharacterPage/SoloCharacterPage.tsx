import React from 'react';
import {Container, LeftBar, ContentBlock, Avatar, Title, ListItem, GoBack} from './styled';
import {NavLink} from "react-router-dom";
import {getConfig} from '@ijl/cli';
import {Character as CharacterDS} from '@main/service/ModelScheme';
import {getApiKey} from '@main/apiKey';
import {connect} from 'react-redux';
import {readOne} from '@main/store/actions/characterActions';

export const SoloCharacterPage: React.FC<ISoloCharacterPageProps> = ({match, character, setCharacter}: ISoloCharacterPageProps) => {

    React.useEffect(() => {
        fetch(`${getConfig()['ii.api.base']}/characters/${match.params.id}?${getApiKey()}`)
            .then(res => res.json())
            .then(res => {
                setCharacter(res.data.results[0] as CharacterDS)
            })
    }, []);

    return (
        <>
            {character.id && (
                <div>
                    <LeftBar>
                        <NavLink to={"/ii/characters"} style={{textDecoration: "none"}}>
                            <GoBack>&lt;- Go back</GoBack>
                        </NavLink>
                        <Avatar>
                            <img width={"100%"} height={"100%"} src={character['thumbnail']['path'] + ".jpg"}/>
                        </Avatar>
                        <Title><b>{character['name']}</b></Title>
                        {character['description'] ?
                            <Title>
                                Description:<br/>
                                <p style={{fontSize: "14px"}}>{character['description']}</p>
                            </Title>
                            : ""}
                    </LeftBar>
                    <Container>
                        {character.comics ?
                            <ContentBlock>
                                <Title><b>Comics</b></Title>
                                {character.comics.items.map((item, index) =>
                                    <a href={'https://www.google.com/search?q=' + item['name'] + ' series'} key={index}
                                       style={{textDecoration: "none"}}>
                                        <ListItem>{item['name']}</ListItem>
                                    </a>
                                )}
                            </ContentBlock>
                            : ""
                        }
                        {character.events ?
                            <ContentBlock>
                                <Title><b>Events</b></Title>
                                {character.events.items.map((item, index) =>
                                    <a href={'https://www.google.com/search?q=' + item['name'] + ' events'} key={index}
                                       style={{textDecoration: "none"}}>
                                        <ListItem>{item['name']}</ListItem>
                                    </a>
                                )}
                            </ContentBlock>
                            : ""
                        }
                        {character.series ?
                            <ContentBlock>
                                <Title><b>Series</b></Title>
                                {character.series.items.map((item, index) =>
                                    <a href={'https://www.google.com/search?q=' + item['name'] + ' series'} key={index}
                                       style={{textDecoration: "none"}}>
                                        <ListItem>{item['name']}</ListItem>
                                    </a>
                                )}
                            </ContentBlock>
                            : ""
                        }
                        {character.stories ?
                            <ContentBlock>
                                <Title><b>Stories</b></Title>
                                {character.stories.items.map((item, index) =>
                                    <a href={'https://www.google.com/search?q=' + item['name'] + ' stories'} key={index}
                                        style={{textDecoration: "none"}}>
                                        <ListItem>{item['name']}</ListItem>
                                    </a>
                                )}
                            </ContentBlock>
                            : ""
                        }
                    </Container>
                </div>
            )}
        </>
    )
}

interface ISoloCharacterPageProps {
    match: any;
    character: CharacterDS;
    setCharacter: Function;
}

export function mapStateToProps(state: any): {character: CharacterDS} {
    return {
        character: state.characters.character,
    }
} 

export function mapDispatchToProps(dispatch: Function): {setCharacter: Function} {
    return {
        setCharacter: (character: CharacterDS) => dispatch(readOne(character))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SoloCharacterPage);