import React from 'react';
import {shallow, mount, configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {SoloCharacterPage, mapDispatchToProps, mapStateToProps} from '../SoloCharacterPage';
import { Character } from '@main/service/ModelScheme';
import { Router } from 'react-router-dom';
import { act } from 'react-dom/test-utils';
import { READ_ONE } from '@main/store/actions/characterActionTypes';

configure({ adapter: new Adapter() });

describe('SoloCharacterPage component testing', () => {
    it('renders without crashing with all data', () => {
        const jsonFile = require('../../../../../stubs/characters.json');
        shallow(<SoloCharacterPage character={jsonFile.data.results[0]} />);
    });

    it('renders without crashing comics/events/series/stories', () => {
        const jsonFile = require('../../../../../stubs/characters.json');
        shallow(<SoloCharacterPage character={{
            ...jsonFile.data.results[0],
            comics: null,
            events: null,
            series: null,
            stories: null,
        }} />);
    });

    it('renders with mocked fetch', () => {
        const jsonFile = require('../../../../../stubs/characters.json');
        const mockSuccessResponse = {data: {results: [...jsonFile.data.results]}};
        // @ts-ignore
        const mockJsonPromise: Promise<Response> = Promise.resolve(mockSuccessResponse);
        // @ts-ignore
        const mockFetchPromise: Promise<Response> = Promise.resolve({
            json: () => mockJsonPromise,
        });
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

        const historyMock = { push: jest.fn(), location: {}, listen: jest.fn() };
        act(() => {
            mount(
                <Router history={historyMock}>
                    <SoloCharacterPage match={{params: {id: '1'}}} character={{id: undefined}} setCharacters={jest.fn()} />
                </Router>
             );
        })

        // @ts-ignore
        global.fetch.mockClear();
        delete global.fetch;
    });

    it('mapStateToProps()', () => {
        const state: any = {
            characters: {
                character: {}
            }
        };

        expect(mapStateToProps(state))
            .toEqual({character: {}});
    });

    it('mapDispatchToProps()', () => {
        const dispatch = jest.fn();
        const character: Character = {id: 0};

        mapDispatchToProps(dispatch).setCharacter(character);

        expect(dispatch.mock.calls[0][0])
            .toEqual({ type: READ_ONE, payload: character});
    });
});
