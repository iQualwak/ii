import styled from 'styled-components';

export const LeftBar = styled.section`
    background-color: #393939;
    
    @media screen and (max-device-width:768px) {
        margin-left: 8px;
        margin-bottom: 8px;
        width: calc(100% - 32px);
        position: relative;
        border-radius: 16px;
        top: 8px;
        padding: 8px;
    }
    
    @media screen and (min-width:769px) {
        width: 200px;
        position: fixed;
        height: 100vh;
        margin-top: -50px;
        z-index: 1000
    }
`;

export const Container = styled.section`
    padding: 8px;
    display: grid;
    grid-gap: 8px;
    grid-template-rows: repeat(auto-fill, 1fr);
    justify-items: center;
    
    @media screen and (max-device-width:768px) {
        margin-left: 0;
        grid-template-columns: repeat(auto-fill, 1fr);
    }
    
    @media screen and (min-width:769px) {
        margin-left: 200px;
        grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
    }
`;

export const ContentBlock = styled.section`
    // max-height: 300px;
    background-color: rgba(80, 80, 80, .5);
    border-radius: 16px;
    padding-bottom: 16px;
    overflow: hidden;
    
    @media screen and (max-device-width:768px) {
        width: 100%;
    }
    
    @media screen and (min-width:769px) {
        width: 200px;
    }
`;

export const Avatar = styled.section`
    margin: 16px;
    margin-left: calc(50% - 84px);
    width: 168px;
    height: 168px;
    border-radius: 100%;
    overflow: hidden;
`;

export const Title = styled.section`
    @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');
    
    color: #fff;
    width: calc(100% - 16px);
    text-align: center;
    font-family: 'Anton', sans-serif;
    font-size: 16px;
    margin-top: 16px;
    margin-bottom: 16px;
    margin-left: 8px;
`;

export const ListItem = styled.section`
    @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');
    
    color: #fff;
    width: calc(100% - 16px);
    text-align: center;
    font-family: 'Anton', sans-serif;
    font-size: 12px;
    // margin-top: 16px;
    text-decoration: none;
    padding: 4px;
    margin: 0 4px;
    border-radius: 4px;
    transition: background-color .2s;
    
    &:hover {
        background-color: rgba(200, 200, 200, .8);
    }
`;

export const GoBack = styled.section`
    @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');
    
    color: #fff;
    width: calc(100% - 40px);
    text-align: center;
    font-family: 'Anton', sans-serif;
    font-size: 16px;
    margin: 16px;
    text-decoration: none;
    padding: 4px;
    border-radius: 4px;
    background-color: rgba(200, 200, 200, .4);
    transition: background-color .2s;
    
    &:hover {
        background-color: rgba(200, 200, 200, .8);
    }
`;