import React from 'react';
import CharactersDeck from './CharactersDeck'
import {Character as CharacterDS} from '../../../service/ModelScheme'

const CharactersAnimationView: React.FC<ICharactersAnimationViewProps> = (props: ICharactersAnimationViewProps) => {
    return (
        <div style={page}>
            <div style={root}>
                <CharactersDeck characters={props.characters.filter(
                    (character: CharacterDS) => {
                        return character.thumbnail.path.split('/').pop() !== 'image_not_available'
                    })} />
            </div>
        </div>
    );
}

const page = {
    overscrollBehaviorY: 'contain',
    margin: 0,
    padding: 0,
    height: '100%',
    width: '100%',
    userSelect: 'none',
    fontFamily:
      '-apple-system, BlinkMacSystemFont, avenir next, avenir, helvetica neue, helvetica, ubuntu, roboto, noto, segoe ui, arial, sans-serif',
    position: 'fixed',
    overflow: 'hidden',
    boxSizing: 'border-box'
}

export const root = {
    background: '#222',
    position: 'fixed',
    overflow: 'hidden',
    width: '100%',
    height: '100%',
    cursor: `url('https://uploads.codesandbox.io/uploads/user/b3e56831-8b98-4fee-b941-0e27f39883ab/Ad1_-cursor.png') 39 39, auto`,
    boxSizing: 'border-box'
}

export interface ICharactersAnimationViewProps {
    characters: CharacterDS[];
}

export default CharactersAnimationView;