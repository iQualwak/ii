import React from 'react';
import {Character as CharacterDS} from '@main/service/ModelScheme'
import {connect} from 'react-redux';
import {movePage, readAll} from '@main/store/actions/characterActions';
import { getApiKey} from '@main/apiKey';
import CharactersCardView from './CharactersCardView';
import CharactersAnimationView from './CharactersAnimationView';

export const CharactersPage: React.FC<ICharactersPageProps> = (props: ICharactersPageProps) => {
    const [searchText, setSearchText] = React.useState("")
    const [numberOfPages, setNumberOfPages] = React.useState(0)
    const [view, setView] = React.useState<CharactersView>(CharactersView.CARD);

    function handleSearchTextChange(searchText: string, characters: CharacterDS[]): void {
        setSearchText(searchText);
        const numberOfElements: number = characters.filter((char) => char.name.includes(searchText) || char.description.includes(searchText)).length;
        setNumberOfPages(Math.ceil(numberOfElements / props.pageLimit))
    }

    React.useEffect(() => {
        fetch(`https://gateway.marvel.com/v1/public/characters?${getApiKey()}&limit=40`)
            .then(res => res.json())
            .then(res => {
                props.readCharacters(res.data.results as CharacterDS[])
                setNumberOfPages(Math.ceil(res.data.results.length / props.pageLimit))
            })
    }, []);

    function changeView(currentView: CharactersView): void {
        switch (currentView) {
            case CharactersView.CARD:
                setView(CharactersView.ANIMATION);
                break;
            case CharactersView.ANIMATION:
                setView(CharactersView.CARD);
                break;
            default:
                break;
        }
    }

    return (
        <>
            <div>
                <p style={{textAlign: 'center'}}>
                    <button 
                        style={{color: 'white', backgroundColor: '#f0141e', border: 0, outline: 'none'}}
                        onClick={(e) => changeView(view)}>
                        CHANGE VIEW
                    </button>
                </p>
                {view === CharactersView.CARD
                ? <CharactersCardView 
                    {...props}
                    searchText={searchText}
                    handleSearchTextChange={handleSearchTextChange}
                    numberOfPages={numberOfPages}
                    setNumberOfPages={setNumberOfPages} />
                : <CharactersAnimationView characters={props.characters} />}
            </div>
        </>
    );
}

export enum CharactersView {
    CARD = "CARD",
    ANIMATION = "ANIMATION",
}

interface ICharactersPageProps {
    characters: CharacterDS[];
    currentPage: number;
    pageLimit: number
    readCharacters: Function;
    movePage: Function;

}

export function mapStateToProps(state: any): { 
        characters: CharacterDS[],
        currentPage: number,
        pageLimit: number 
    } {
    return {
        characters: state.characters.characters,
        currentPage: state.characters.currentPage,
        pageLimit: state.characters.pageLimit
    }
}

export function mapDispatchToProps(dispatch: Function): { readCharacters: Function, movePage: Function } {
    return {
        readCharacters: (characters) => dispatch(readAll(characters)),
        movePage: (page) => dispatch(movePage(page))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CharactersPage);