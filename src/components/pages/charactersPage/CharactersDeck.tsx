import { Character } from '@main/service/ModelScheme'
import React, { useState } from 'react'
// @ts-ignore
import { useSprings, animated, interpolate } from 'react-spring'
import { useGesture } from 'react-use-gesture';
import { useHistory } from 'react-router-dom';
// import {jest} from '@jest/globals'
// import jest from 'jest';

const to = (i) => ({ x: 0, y: i * -4, scale: 1, rot: -10 + Math.random() * 20, delay: i * 100 })
const from = (i) => ({ x: 0, rot: 0, scale: 1.5, y: -1000 })
const trans = (r, s) => `perspective(1500px) rotateX(30deg) rotateY(${r / 10}deg) rotateZ(${r}deg) scale(${s})`

const CharactersDeck: React.FC<ICharactersDeckProps> = ({characters = []}: ICharactersDeckProps) => {
  const [gone] = useState(() => new Set())
  const [props, set] = useSprings(characters.length, (i) => ({ ...to(i), from: from(i) }))
    // @ts-ignore
    const bind = useGesture(({ args: [index], down, delta: [xDelta], distance, direction: [xDir], velocity }) => {
    const trigger = velocity > 0.2
    const dir = xDir < 0 ? -1 : 1
    if (!down && trigger) gone.add(index)
    // @ts-ignore
    set((i) => {
      if (index !== i) return
      const isGone = gone.has(index)
      const x = isGone ? (200 + window.innerWidth) * dir : down ? xDelta : 0
      const rot = xDelta / 100 + (isGone ? dir * 10 * velocity : 0)
      const scale = down ? 1.1 : 1
      return { x, rot, scale, delay: undefined, config: { friction: 50, tension: down ? 800 : isGone ? 200 : 500 } }
    })
    if (!down && gone.size === characters.length) setTimeout(() => {
        // @ts-ignore
        return gone.clear() || set((i) => to(i));
    }, 600)
  });
  React.useEffect(() => {}, [characters]);

  const history = useHistory();

  return props.map(({ x, y, rot, scale }, i: number) => (
    <React.Fragment key={i}>
        {characters[i] && (
            <animated.div
              key={characters[i].id}
              onContextMenu={(event) => {
                event.preventDefault();
                history.push('/ii/characters/' + characters[i].id)
              }}
              style={{ ...divCardStyle, transform: interpolate([x, y], (x, y) => `translate3d(${x}px,${y}px,0)`) }}>
                  <animated.div
                    key={characters[i].id}
                    {...bind(i)}
                    style={{ ...cardStyle, transform: interpolate([rot, scale], trans), backgroundImage: `url(${characters[i].thumbnail.path + ".jpg"})` }}
                  />
            </animated.div>
        )}
    </React.Fragment>
  ))
}



export const cardStyle = {
    backgroundColor: 'white',
    backgroundSize: 'auto 85%',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    width: '45vh',
    maxWidth: '300px',
    height: '85vh',
    maxHeight: '570px',
    willChange: 'transform',
    borderRadius: '10px',
    boxShadow: '0 12.5px 100px -10px rgba(50, 50, 73, 0.4), 0 10px 10px -10px rgba(50, 50, 73, 0.3)'
  }
  
  export const divCardStyle = {
    position: 'absolute',
    width: '100vw',
    height: '100vh',
    willChange: 'transform',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  }

interface ICharactersDeckProps {
    characters: Character[];
}

export default CharactersDeck;
