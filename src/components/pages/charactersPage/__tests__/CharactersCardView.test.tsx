import React from 'react';
import {shallow, mount, configure, ReactWrapper, ShallowWrapper} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import CharactersCardView from '../CharactersCardView';
import {Router, NavLink} from 'react-router-dom';

configure({ adapter: new Adapter() });

describe('CharactersCardView component testing', () => {
    it('renders without crashing', () => {
        shallow(
            <CharactersCardView 
                characters={[{name: 'name', description: 'descr', thumbnail: {path: ''}}]}
                currentPage={0}
                pageLimit={5}
                readCharacters={jest.fn()}
                movePage={jest.fn()}
                searchText={''}
                handleSearchTextChange={jest.fn()}
                numberOfPages={1}
                setNumberOfPages={jest.fn()}
            />
        );
    });

    it('search testing', () => {
        const mockHandleSearchTextChange = jest.fn();

        const wrapper: ShallowWrapper = shallow(
            <CharactersCardView 
                characters={[]}
                currentPage={0}
                pageLimit={5}
                readCharacters={jest.fn()}
                movePage={jest.fn()}
                searchText={''}
                handleSearchTextChange={mockHandleSearchTextChange}
                numberOfPages={1}
                setNumberOfPages={jest.fn()}
            />
        );

        expect(wrapper.find('input')).toHaveLength(1);
        const searchInput = wrapper.find('input').at(0);

        expect(mockHandleSearchTextChange).toHaveBeenCalledTimes(0);
        searchInput.simulate('change', { target: { value: '1' } });
        expect(mockHandleSearchTextChange).toHaveBeenCalled();
        expect(mockHandleSearchTextChange).toHaveBeenCalledTimes(1);
        expect(mockHandleSearchTextChange).toHaveBeenCalledWith('1', []);
    });

    it('movePage testing', () => {
        const mockMovePage = jest.fn();

        const wrapper: ShallowWrapper = shallow(
            <CharactersCardView 
                characters={[]}
                currentPage={1}
                pageLimit={5}
                readCharacters={jest.fn()}
                movePage={mockMovePage}
                searchText={''}
                handleSearchTextChange={jest.fn()}
                numberOfPages={1}
                setNumberOfPages={jest.fn()}
            />
        );

        expect(wrapper.find('button')).toHaveLength(3);
        const prevPageButton = wrapper.find('button').at(0);

        prevPageButton.simulate('click');
        expect(mockMovePage).toHaveBeenCalled();
        expect(mockMovePage).toHaveBeenCalledWith(1 - 1);
    });
});