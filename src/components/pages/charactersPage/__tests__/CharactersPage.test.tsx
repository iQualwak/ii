import React from 'react';
import {shallow, mount, configure, ShallowMapper, ReactWrapper} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

jest.mock('react-spring', () => ({
    useSprings: jest.fn().mockImplementation(() => [[{x: 0, y: 0, rot: 0, scale: 0}], jest.fn()]),
}));

import {CharactersPage, mapStateToProps, mapDispatchToProps} from '../CharactersPage';
import { READ_ALL } from '@main/store/actions/characterActionTypes';
import { Character } from '@main/service/ModelScheme';
import CharactersCardView from '../CharactersCardView';
import CharactersAnimationView from '../CharactersAnimationView';

configure({ adapter: new Adapter() });

describe('CharactersPage component testing', () => {
    it('renders without crashing', () => {
        shallow(<CharactersPage characters={[{'id': '0', 'thumbnail': {'path': '/', 'name': 'cap'}}]} />);
    });

    it('verify initial view (either CARD or ANIMATION)', () => {
        const wrapper: ShallowMapper = shallow(<CharactersPage characters={[]} />);

        expect(wrapper.find(CharactersCardView)).toHaveLength(1);
        expect(wrapper.find(CharactersAnimationView)).toHaveLength(0);
    });


    it('change view (either CARD or ANIMATION)', () => {
        const mockSuccessResponse = {data: {results: []}};
        // @ts-ignore
        const mockJsonPromise: Promise<Response> = Promise.resolve(mockSuccessResponse);
        // @ts-ignore
        const mockFetchPromise: Promise<Response> = Promise.resolve({
            json: () => mockJsonPromise,
        });
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

        const wrapper: ReactWrapper = mount(<CharactersPage characters={[]} />);

        const changeViewButton = wrapper.find('button').at(0);

        expect(wrapper.find(CharactersCardView)).toHaveLength(1);
        expect(wrapper.find(CharactersAnimationView)).toHaveLength(0);

        changeViewButton.simulate('click');

        expect(wrapper.find(CharactersCardView)).toHaveLength(0);
        expect(wrapper.find(CharactersAnimationView)).toHaveLength(1);

        // and back
        changeViewButton.simulate('click');

        expect(wrapper.find(CharactersCardView)).toHaveLength(1);
        expect(wrapper.find(CharactersAnimationView)).toHaveLength(0);
        
        // @ts-ignore
        global.fetch.mockClear();
        delete global.fetch;
    });


    it('search change', () => {
        const jsonFile = require('../../../../../stubs/characters.json');
        const mockSuccessResponse = {data: {results: [...jsonFile.data.results]}};
        // @ts-ignore
        const mockJsonPromise: Promise<Response> = Promise.resolve(mockSuccessResponse);
        // @ts-ignore
        const mockFetchPromise: Promise<Response> = Promise.resolve({
            json: () => mockJsonPromise,
        });
        global.fetch = jest.fn().mockImplementation(() => mockFetchPromise);

        const wrapper: ReactWrapper = mount(<CharactersPage characters={[]} />);

        const searchInput = wrapper.find('input').at(0);

        searchInput.simulate('change', { target: { value: 'Ad' } });

        // @ts-ignore
        global.fetch.mockClear();
        delete global.fetch;
    });

    it('mapStateToProps()', () => {
        const state: any = {
            characters: {
                characters: []
            }
        };

        expect(mapStateToProps(state))
            .toEqual({characters: []});
    });

    it('mapDispatchToProps()', () => {
        const dispatch = jest.fn();
        const characters: Character[] = [{id: 0}];

        mapDispatchToProps(dispatch).readCharacters(characters);

        expect(dispatch.mock.calls[0][0])
            .toEqual({ type: READ_ALL, payload: characters});
    });
});