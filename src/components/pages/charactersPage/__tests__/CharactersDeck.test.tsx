import React from 'react';
import { mount, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

jest.mock('react-spring', () => ({
    useSprings: jest.fn().mockImplementation(() => [[{x: 0, y: 0, rot: 0, scale: 0}], jest.fn()]),
}));

import CharactersDeck from '../CharactersDeck';

configure({ adapter: new Adapter() });

describe('CharactersDeck component testing', () => {
   it('renders without crashing', () => {
        mount(<CharactersDeck characters={[]} />);
    });
});