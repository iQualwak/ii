import React from 'react';
import {shallow, mount, configure, ReactWrapper} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Router } from 'react-router-dom';
import CharactersAnimationView from '../CharactersAnimationView';
import { Character } from '@main/service/ModelScheme';

configure({ adapter: new Adapter() });

describe('CharactersAnimationView component testing', () => {
    const characters: Character[] = [
        {
            thumbnail: {
                path: 'image_not_found'
            },
        },
        {
            thumbnail: {
                path: 'spider-man'
            },
        }
    ];

    it('renders without crashing', () => {
        shallow(<CharactersAnimationView characters={characters} />);
    });
});