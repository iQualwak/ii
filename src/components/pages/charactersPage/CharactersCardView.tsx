import React from 'react';
import {Container, Character, CharacterName, PageButton, Label, ButtonsContainer, SearchTitle} from './styled';
import {NavLink} from "react-router-dom";
import {Character as CharacterDS} from '@main/service/ModelScheme'

const CharactersCardView: React.FC<ICharactersCardViewProps> = (props: ICharactersCardViewProps) => {
    if (props.currentPage >= props.numberOfPages) {
        props.movePage(props.numberOfPages - 1)
    }
 
     let pages = [];
 
     let i = 0;
     while (i < props.numberOfPages) {
         pages.push({index: i, isActive: props.currentPage === i})
         i += 1
     }
 
     return (
         <div>
             <Container>
                 {props
                     .characters
                     .filter((char) => char.name.includes(props.searchText) || char.description.includes(props.searchText)) // search
                     .slice(...getSlice(props.currentPage, props.pageLimit)) // pagination
                     .map((character: CharacterDS, index: number) =>
                         <NavLink to={`/ii/characters/${character.id}`} key={index}>
                             <Character>
                                 <img src={character.thumbnail.path + ".jpg"}
                                      style={{height: '150px', width: '150px'}}/>
                                 <CharacterName>{character.name}</CharacterName>
                             </Character>
                         </NavLink>
                     )}
             </Container>,
             <Label>
                 <SearchTitle>Search me:</SearchTitle><br/>
                 <input type="text" value={props.searchText} onChange={(e) => props.handleSearchTextChange(e.target.value, props.characters)}
                     placeholder={"Example: 3-D Man"}
                 />
             </Label>,
             <ButtonsContainer>
                 <button onClick={() => props.movePage(props.currentPage - 1)}>{"<"}</button>
                 {pages.map((page, index) => {
                     return (
                         <button 
                            key={index}
                            style={{ backgroundColor: page.isActive ? "GREY" : "WHITE" }}
                            onClick={() => props.movePage(index)}>{index}</button>
                     )
                 })}
                 <button onClick={() => props.movePage(props.currentPage + 1)}>{">"}</button>
             </ButtonsContainer>
         </div>
     );
}

function getSlice(current_page: number, page_limit: number): [number, number] {

    return [current_page * page_limit, current_page * page_limit + page_limit]

}

export interface ICharactersCardViewProps {
    characters: CharacterDS[];
    currentPage: number;
    pageLimit: number
    readCharacters: Function;
    movePage: Function;
    searchText: string;
    handleSearchTextChange: Function;
    numberOfPages: number;
    setNumberOfPages: Function;
}

export default CharactersCardView;