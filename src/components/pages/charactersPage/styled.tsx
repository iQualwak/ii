import styled from 'styled-components';

export const Container = styled.section`
    padding: 8px;
    display: grid;
    grid-gap: 8px;
    grid-template-rows: repeat(auto-fill, 150px);
    grid-template-columns: repeat(auto-fill, minmax(150px, 1fr));
    justify-items: center;
`;

export const Character = styled.section`
    cursor: pointer;
    width: 150px;
    height: 150px;
    overflow: hidden;
    transition: border-radius .2s;
    border-radius: 75px;
    box-shadow: inset 0 4px 8px 0 #9f9f9f;
    
    &:hover {
        border-radius: 16px;
    }
`;

export const CharacterName = styled.section`
    @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');

    position: absolute;
    width: 150px;
    height: 150px;
    transition: box-shadow .2s, margin-top .2s, border-radius .2s;
    text-align: center;
    color: #fff;
    margin-top: -154px;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    font-family: 'Anton', sans-serif;
    font-size: 16px;
    border-radius: 75px;
    opacity: 0;
    transition: border-radius .2s, opacity .2s;
    background-color: rgba(10, 10, 10, .7);
    
    &:hover {
        border-radius: 15px;
        opacity: 1;
    }
`;

export const CharProfile = styled.section`
    position: absolute;
    width: 300px;
    height: 300px;
    transition: box-shadow .2s, margin-top .2s, border-radius .2s;
    text-align: center;
    color: green;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
    font-family: 'Open Sans', sans-serif;
    font-size: 16px;
    border-radius: 75px;
    opacity: 100;
    transition: border-radius .2s, opacity .2s;
    background-color: rgba(10, 10, 10, .7);
    
    &:hover {
        border-radius: 15px;
        opacity: 1;
    }
`;

export const PageButton = styled.section`
  transition-duration: 0.4s;
  display: block;

&:hover {
  background-color: #4CAF50; /* Green */
  color: white;
}
`;

export const Label = styled.section`
  text-align: center;
  width: 100%;
  
  & input {
    @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');
  
    font-family: 'Anton', sans-serif;
    background-color: #fff;
    padding: 8px;
    text-align: center;
    border-radius: 32px;
    border: none;
  }
`;

export const ButtonsContainer = styled.section`
  width: 100%;
  text-align: center;
  
  & button {
    border-radius: 32px;
    width: 32px;
    height: 32px;
    background-color: #fff;
    cursor: pointer;
    border: none;
    margin: 3px;
    color: red;
    transition-duration: 0.4s;
    margin-left: 8px;
  }
`;

export const SearchTitle = styled.section`
  @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');
  
  color: #fff;
  font-family: 'Anton', sans-serif;
`;