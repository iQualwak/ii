import React from 'react';
import {useHistory} from 'react-router-dom';

const HomePage: React.FC = () => {
    const history = useHistory(); // todo add type
    
    return (
        <>
            <div style={{textAlign: 'center', color: 'white'}}>
                <br />
                <h1>Welcome to the Marvel Characters Library!</h1>
                <br />
                <button 
                    style={{width: 100, height: 50, color: 'white', backgroundColor: '#f0141e', border: 0}}
                    onClick={() => history.push('/ii/characters')}>
                    Start!
                </button>
            </div>
        </>
    );
}

export default HomePage;