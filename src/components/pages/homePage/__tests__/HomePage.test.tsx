import React from 'react';
import {shallow, mount, configure, ReactWrapper} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import HomePage from '../HomePage';
import { Router } from 'react-router-dom';

configure({ adapter: new Adapter() });

describe('HomePage component testing', () => {
    it('renders without crashing', () => {
        shallow(<HomePage />);
    });

    it('only one start button exists and is not disabled', () => {
        const wrapper: ReactWrapper = mount(<HomePage />);
        
        const buttons = wrapper.find('button');
        expect(buttons).toHaveLength(1);

        expect(buttons.at(0).props().disabled).toEqual(undefined); // because there are no such prop
    });

    it('start button press changes history path', () => {
        const historyMock = { push: jest.fn(), location: {}, listen: jest.fn() };

        const wrapper: ReactWrapper = mount(
            <Router history={historyMock}>
                <HomePage />
            </Router>
        );

        const button = wrapper.find('button').at(0);

        // check that push() haven't been called
        expect(historyMock.push).toHaveBeenCalledTimes(0);

        // clicks button that trigger push() method
        button.simulate('click');

        expect(historyMock.push).toHaveBeenCalled();
        expect(historyMock.push).toHaveBeenCalledTimes(1);
    });
});