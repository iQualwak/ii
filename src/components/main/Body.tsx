import React from 'react';
import CharactersPage from "@main/components/pages/charactersPage/CharactersPage";
import SoloCharacterPage from "@main/components/pages/soloCharacterPage/SoloCharacterPage";
import HomePage from '@main/components/pages/homePage/HomePage';
import {Container} from '../styled';
import {Route, BrowserRouter} from "react-router-dom";

const Body: React.FC = () => (
    <Container>
        <BrowserRouter>
            <Route exact path={'/ii/'} component={HomePage} />
            <Route exact path={'/ii/characters'} component={CharactersPage} />
            <Route exact path={'/ii/characters/:id'} component={SoloCharacterPage} />
        </BrowserRouter>
    </Container>
);

export default Body;