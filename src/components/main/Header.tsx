import React from 'react';
import { HeaderBlock } from '../styled';

const Header: React.FC = () => {
    return (
        <HeaderBlock>
            <img src={"https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/MarvelLogo.svg/1200px-MarvelLogo.svg.png"} />
        </HeaderBlock>
    );
};

export default Header;