import styled from 'styled-components';


export const Container = styled.section`
    min-height: calc(100vh - 66px);
    background: #222;
    padding-top: 50px;
    padding-bottom: 16px;
`;

export const FooterBlock = styled.section`
    @import url('https://fonts.googleapis.com/css2?family=Anton&display=swap');

    display: block;
    padding: 16px;
    background-color: #000;
    position: relative;
    bottom: 0;
    left: 0;
    width: calc(100% - 32px);
    transition: padding-top .75s;
    text-align: center;
    font-family: 'Anton', sans-serif;
    overflow: hidden;
    height: 50px;
  
    &:hover {        
        padding-top: 110px;
    }
    
    #about {
        color: white;
    }
    
    #sup {   
        color: rgb(237, 29, 36);
        font-size: 50px;
        margin-top: -150px;
    }
`;

export const HeaderBlock = styled.section`
    position: fixed;
    width: 100%;
    height: 50px;
    background-color: rgba(34, 34, 34, .85);
    z-index: 1000;
    
    & img {
        height: 34px;
        width: 85px;
        margin-left: calc(50% - 42px);
        margin-top: 8px;
    }
`;