
const public_key = "f7710c1412ee11feca149189733ffc7f"
const private_key = "f0f11df0fa89aacddd515bb0abcbee866a742f4c"
import md5 from 'md5';

/**
 * Returns API_KEY by PU and PR
 */
export function getApiKey() {
    let ts = Math.random() * 1000
    return "ts=" + ts.toString() + "&apikey=" + public_key + "&hash=" + md5(ts.toString() + private_key + public_key);
}