import {Character} from "@main/service/ModelScheme";
import {CLEAR_CURRENT, MOVE_PAGE, READ_ALL, READ_ONE} from "../actions/characterActionTypes";
import ReducerAction from "../ReducerAction";

function characterReducer(state = initState, action: ReducerAction): ICharactersState {
    switch (action.type) {
        case READ_ALL:
            return {
                ...state,
                characters: action.payload
            }
        case READ_ONE:
            return {
                ...state,
                character: action.payload
            }
        case CLEAR_CURRENT:
            return {
                ...state,
                character: {}
            } as ICharactersState;
        case MOVE_PAGE:
            return {
                ...state,
                currentPage: action.payload < 0 ? 0 : action.payload
            }
        default:
            return state;
    }
}

export const initState: ICharactersState = {
    characters: [],
    character: {},
    currentPage: 0,
    pageLimit: 20,
}

export interface ICharactersState {
    characters: Character[];
    character: Character;
    currentPage: number;
    pageLimit: number;
}

export default characterReducer;