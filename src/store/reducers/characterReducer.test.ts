import { CLEAR_CURRENT, MOVE_PAGE, READ_ALL, READ_ONE } from '../actions/characterActionTypes';
import ReducerAction from '../ReducerAction';
import characterReducer, {ICharactersState, initState} from './characterReducer';

describe('characterReducer testing', () => {
    it('undefined state, unknown action', () => {
        const action: ReducerAction = {
            type: 'PLEASE_TELL_ME_SOMETHING',
        };
        const stateAfter: ICharactersState = initState;

        expect(characterReducer(undefined, action))
            .toEqual(stateAfter);
    });

    it('undefined state, known action', () => {
        const action: ReducerAction = {
            type: READ_ALL,
            payload: [{id: 1}]
        };
        const stateAfter: ICharactersState = {
            ...initState,
            characters: [{id: 1}]
        };

        expect(characterReducer(undefined, action))
            .toEqual(stateAfter);
    });

    it('READ_ALL', () => {
        const stateBefore: ICharactersState = {
            ...initState
        };

        const action: ReducerAction = {
            type: READ_ALL,
            payload: [{id: 1,}, {id: 2}]
        };

        const stateAfter: ICharactersState = {
            ...initState,
            characters: [{id: 1,}, {id: 2}]
        };

        expect(characterReducer(stateBefore, action))
            .toEqual(stateAfter);
    });

    it('READ_ONE', () => {
        const stateBefore: ICharactersState = {
            ...initState
        };

        const action: ReducerAction = {
            type: READ_ONE,
            payload: {id: 1}
        };

        const stateAfter: ICharactersState = {
            ...initState,
            character: {id: 1}
        };

        expect(characterReducer(stateBefore, action))
            .toEqual(stateAfter);
    });

    it('CLEAR_CURRENT', () => {
        const stateBefore: ICharactersState = {
            ...initState,
            character: {id: 1}
        };

        const action: ReducerAction = {
            type: CLEAR_CURRENT
        };

        const stateAfter: ICharactersState = {
            ...initState
        };

        expect(characterReducer(stateBefore, action))
            .toEqual(stateAfter);
    });

    describe('MOVE_PAGE', () => {
        it('page number is negative', () => {
            const stateBefore: ICharactersState = {
                ...initState,
            };

            const action: ReducerAction = {
                type: MOVE_PAGE,
                payload: -1
            };

            const stateAfter: ICharactersState = {
                ...initState,
            };

            expect(characterReducer(stateBefore, action))
                .toEqual(stateAfter);
        });

        it('page number is greater or equal to 0', () => {
            const stateBefore: ICharactersState = {
                ...initState,
            };

            const action: ReducerAction = {
                type: MOVE_PAGE,
                payload: 10
            };

            const stateAfter: ICharactersState = {
                ...initState,
                currentPage: 10
            };

            expect(characterReducer(stateBefore, action))
                .toEqual(stateAfter);
        });
    });
});