import { Character } from "@main/service/ModelScheme";
import ReducerAction from "../ReducerAction";
import {CLEAR_CURRENT, MOVE_PAGE, READ_ALL, READ_ONE} from "./characterActionTypes";

export function readAll(characters: Character[]): ReducerAction {
    return {
        type: READ_ALL,
        payload: characters,
    }
}

export function readOne(character: Character): ReducerAction {
    return {
        type: READ_ONE,
        payload: character,
    }
}


export function movePage(currentPage: number): ReducerAction {
    return {
        type: MOVE_PAGE,
        payload: currentPage
    }
}


export function clearCurrent(): ReducerAction {
    return {
        type: CLEAR_CURRENT
    }
}