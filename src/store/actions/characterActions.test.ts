import { Character } from "@main/service/ModelScheme";
import ReducerAction from "../ReducerAction";
import { clearCurrent, movePage, readAll, readOne } from "./characterActions";
import { CLEAR_CURRENT, MOVE_PAGE, READ_ALL, READ_ONE } from "./characterActionTypes";

describe('characterActions test', () => {
    it('readAll() action', () => {
        const characters: Character[] = [{id: 1}, {id: 2}];
        
        const expectedAction: ReducerAction = {
            type: READ_ALL,
            payload: characters
        };

        expect(readAll(characters))
            .toEqual(expectedAction);
    });

    it('readOne() action', () => {
        const character: Character = {id: 1};
        
        const expectedAction: ReducerAction = {
            type: READ_ONE,
            payload: character
        };

        expect(readOne(character))
            .toEqual(expectedAction);
    });

    it('clearCurrent() action', () => {
        const expectedAction: ReducerAction = {
            type: CLEAR_CURRENT,
        };

        expect(clearCurrent())
            .toEqual(expectedAction);
    });

    it('movePage() action', () => {
        const expectedAction: ReducerAction = {
            type: MOVE_PAGE,
            payload: 0
        };

        expect(movePage(0))
            .toEqual(expectedAction);
    });
});