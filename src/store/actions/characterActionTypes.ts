export const READ_ALL: string = 'READ_ALL';
export const READ_ONE: string = 'READ_ONE';
export const MOVE_PAGE: string = 'MOVE_PAGE';
export const CLEAR_CURRENT: string = 'CLEAR_CURRENT';