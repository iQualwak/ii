import {
    createStore,
    Store,
    Reducer,
    combineReducers,
} from 'redux';
import characterReducer from './reducers/characterReducer';

const initialState: any = {}

const combinedReducers: Reducer<any, any> = combineReducers({
    characters: characterReducer,
});

const applicationStore: Store = createStore(
    combinedReducers,
    initialState,
);

export default applicationStore;